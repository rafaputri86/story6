from django.test import TestCase, Client
from django.urls import resolve
from . import models, forms, views
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

class TestStatusPage(TestCase) :
    def test_Apakah_ada_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_url_is_not_exist(self):
        response = Client().get('')
        self.assertFalse(response.status_code == 404)

    def test_status_post_sukses(self):
        response_post = Client().post('/',{'Comment':'coba coba'})
        self.assertEqual(response_post.status_code, 302)

    def test_func(self):
        found = resolve('/') 
        self.assertEqual(found.func, views.status)


    def test_apakah_ada_html_status(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')
        
    def test_form_apakah_ada_isi_status(self):
        contoh = 'berhasil'
        form_input = {
            'Comment' :contoh
        }
        test = forms.CommentForm(data = form_input)
        self.assertTrue(test.is_valid())

    def test_model_apakah_isi_dalam_status(self):
        hitung = models.Masukan.objects.all().count()
        Comment_model = models.Masukan.objects.create(Comment="test")
        hitung1 = models.Masukan.objects.all().count()
        self.assertEqual(hitung, 0)
        self.assertEqual(hitung1, hitung+1)
        self.assertTrue(isinstance(Comment_model, models.Masukan))

    def test_apakah_ada_button_status(self):
        response = Client().get('')
        content = response.content.decode('utf8') 
        self.assertIn ("<button", content)
        self.assertIn("Status", content)

class FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(25)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_post(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.assertIn('Rafa Status', self.browser.title)

        text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Halo, Apa Kabar?', text)

        inputbox1 = self.browser.find_element_by_id('id_Comment')
        time.sleep(1)
        inputbox1.send_keys('coba coba')
        time.sleep(2)
        inputbox1.send_keys(Keys.ENTER)
        time.sleep(5)

        self.assertIn('coba coba', self.browser.page_source)
